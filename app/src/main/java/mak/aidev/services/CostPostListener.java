package mak.aidev.services;

import mak.aidev.dataobjects.ServerReply;

/**
 * Created by william on 3/16/16.
 */
public interface CostPostListener {
    void onCostPost(ServerReply serverReply);

    void onNetworkFailure(String localizedMessage);
}
