package mak.aidev.services;

import mak.aidev.dataobjects.Diagnosis;

/**
 * Created by william on 3/16/16.
 */
public interface DiagnosisListener {
    void onDiagnosisResult(Diagnosis diagnosis);

    void onNetworkFailure(String localizedMessage);
}
