package mak.aidev.services;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

import java.io.File;

import mak.aidev.dataobjects.CostData;
import mak.aidev.dataobjects.Diagnosis;
import mak.aidev.dataobjects.ServerReply;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by william on 3/3/16.
 */
public class AiDevServiceHandler {
    private AiDevService aiDevService;
    private DiagnosisListener diagnosisListener;
    private CostPostListener costPostListener;



    public AiDevServiceHandler(AiDevService aiDevService){
        this.aiDevService = aiDevService;
    }

    public void diagnoseImage(File file, DiagnosisListener diagnosisListener){
       this.diagnosisListener = diagnosisListener;

        MediaType MEDIA_TYPE_JPEG = MediaType.parse("image/jpeg");
        RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JPEG, file);
        aiDevService.archiveUpload(requestBody, new DiagnosisCallback());

    }

    public void sendCostInformation(CostData costData, CostPostListener costPostListener){
        this.costPostListener = costPostListener;
        aiDevService.sendCosts(costData, new CostPostCallback());
    }


    private class DiagnosisCallback implements Callback<Diagnosis> {
        @Override
        public void success(Diagnosis diagnosis, Response response) {
            diagnosisListener.onDiagnosisResult(diagnosis);
        }

        @Override
        public void failure(RetrofitError error) {
            diagnosisListener.onNetworkFailure(error.getLocalizedMessage());
        }
    }

    private class CostPostCallback implements Callback<ServerReply> {
        @Override
        public void success(ServerReply serverReply, Response response) {
            costPostListener.onCostPost(serverReply);
        }

        @Override
        public void failure(RetrofitError error) {
            costPostListener.onNetworkFailure(error.getLocalizedMessage());
        }
    }
}
