package mak.aidev.services;

import com.squareup.okhttp.RequestBody;

import mak.aidev.dataobjects.CostData;
import mak.aidev.dataobjects.Diagnosis;
import mak.aidev.dataobjects.ServerReply;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;

/**
 * Created by william on 3/3/16.
 */
public interface AiDevService {
    @Multipart
    @POST("/image_analysis")
    void archiveUpload(@Part("image")RequestBody requestBody, Callback<Diagnosis> cb);

    @POST("/cost_post")
    void sendCosts(@Body CostData costData, Callback<ServerReply> cb);

}
