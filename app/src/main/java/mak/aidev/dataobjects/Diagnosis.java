package mak.aidev.dataobjects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by william on 3/16/16.
 */
public class Diagnosis implements Serializable{
    public String getHealthy() {
        return healthy;
    }

    public String getCbb() {
        return cbb;
    }

    public String getCbsd() {
        return cbsd;
    }

    public String getCmd() {
        return cmd;
    }

    @SerializedName("healthy")
    private String healthy;
    @SerializedName("cbb")
    private String cbb;
    @SerializedName("cbsd")
    private String cbsd;
    @SerializedName("cmd")
    private String cmd;
}
