package mak.aidev.dataobjects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by william on 3/3/16.
 */
public class ServerReply implements Serializable{
    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;


}
