package mak.aidev.dataobjects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by william on 3/16/16.
 */
public class CostData implements Serializable{
    public CostData(int salesCost, int stemTransportationCost, int tuberTransportationCost, int inspectionCost) {
        this.salesCost = salesCost;
        this.stemTransportationCost = stemTransportationCost;
        this.tuberTransportationCost = tuberTransportationCost;
        this.inspectionCost = inspectionCost;
    }
    @SerializedName("sales_cost")
    private int salesCost;
    @SerializedName("stem_transport_cost")
    private int stemTransportationCost;
    @SerializedName("tuber_transport_cost")
    private int tuberTransportationCost;
    @SerializedName("inspection_cost")
    private int inspectionCost;
}
