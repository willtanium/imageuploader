package mak.aidev.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import mak.aidev.dataobjects.CostData;
import mak.aidev.imageuploader.MainActivity;
import mak.aidev.imageuploader.R;

/**
 * Created by william on 3/16/16.
 */
public class CostIntakeForm extends Fragment implements View.OnClickListener, Validator.ValidationListener {
    @NotEmpty
    private EditText salesCostEntry;
    @NotEmpty
    private EditText stemTransportCostEntry;
    @NotEmpty
    private EditText tuberTransportCostEntry;
    @NotEmpty
    private EditText inspectionCostEntry;
    private Button sendDataBtn;
    private Button cancelBtn;
    private Validator validator;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle){
        View view = inflater.inflate(R.layout.cost_intake_layout, root, false);
        salesCostEntry = (EditText)view.findViewById(R.id.sale_cost);
        stemTransportCostEntry = (EditText)view.findViewById(R.id.stem_transportation);
        tuberTransportCostEntry = (EditText)view.findViewById(R.id.tuber_transport_cost);
        inspectionCostEntry = (EditText)view.findViewById(R.id.inspection_cost);
        sendDataBtn = (Button)view.findViewById(R.id.send_button);
        cancelBtn = (Button)view.findViewById(R.id.cancel_button);
        sendDataBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.send_button:{
                validator.validate();
                break;}
            case R.id.cancel_button:{ ((MainActivity)getActivity())
                    .shiftFragment(new DefaultView());break;}
        }
    }

    @Override
    public void onValidationSucceeded() {
        int salesCost = Integer.parseInt(salesCostEntry.getText().toString());
        int stemTransportationCost = Integer.parseInt(stemTransportCostEntry.getText().toString());
        int tuberTransportationCost = Integer.parseInt(tuberTransportCostEntry.getText().toString());
        int inspectionCost = Integer.parseInt(inspectionCostEntry.getText().toString());
        CostData costData = new CostData(salesCost, stemTransportationCost, tuberTransportationCost,
                inspectionCost);
                ((MainActivity) getActivity()).sendCostInformation(costData);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {

            }
        }
    }
}
