package mak.aidev.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import mak.aidev.imageuploader.MainActivity;
import mak.aidev.imageuploader.R;

/**
 * Created by william on 3/16/16.
 */
public class DefaultView extends Fragment implements View.OnClickListener {
    private Button costFormBtn;
    private Button imageUploadBtn;
    private View view;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle){
        view = inflater.inflate(R.layout.default_layout, root, false);
        costFormBtn = (Button)view.findViewById(R.id.form_post);
        imageUploadBtn = (Button)view.findViewById(R.id.image_upload);
        costFormBtn.setOnClickListener(this);
        imageUploadBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.form_post:{ ((MainActivity)getActivity())
                    .shiftFragment(new CostIntakeForm());break;}
            case R.id.image_upload:{ ((MainActivity)getActivity())
                    .shiftFragment(new ImageTakeFragment());break;}
        }
    }
}
