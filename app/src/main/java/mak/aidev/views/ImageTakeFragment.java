package mak.aidev.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mak.aidev.imageuploader.MainActivity;
import mak.aidev.imageuploader.R;

/**
 * Created by william on 3/3/16.
 */
public class ImageTakeFragment extends Fragment implements View.OnClickListener {
    private ImageView previewImage;
    private Button sendImageBtn;
    private Button cancelBtn;
    static final int REQUEST_TAKE_PHOTO = 11111;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle bundle) {
      View  view = inflater.inflate(R.layout.capture_view, root, false);
        sendImageBtn = (Button)view.findViewById(R.id.send_button);
        cancelBtn = (Button)view.findViewById(R.id.cancel_button);
        previewImage = (ImageView)view.findViewById(R.id.image_view);
        sendImageBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);
        previewImage.setOnClickListener(this);

        return view;
    }


    private void takePhoto(){
        Context context = getActivity();
        PackageManager packageManager = context.getPackageManager();
        if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
            Toast.makeText(getActivity(), "This device does not have a camera.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }
        // Camera exists? Then proceed...
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        MainActivity activity = (MainActivity)getActivity();
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go.
            // If you don't do this, you may get a crash in some devices.
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(activity, "There was a problem saving the photo...", Toast.LENGTH_SHORT);
                toast.show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri fileUri = Uri.fromFile(photoFile);
                activity.setCapturedImageURI(fileUri);
                activity.setCurrentPhotoPath(fileUri.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        activity.getCapturedImageURI());
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private boolean isPhotoTaken(){
        try{
            String imageURI = ((MainActivity)getActivity()).getCapturedImageURI().toString();

            if(imageURI.contains(".jpg")){
                Log.i("IMAGE", String.format("Image is at %s", imageURI));
                return true;
            }}catch (NullPointerException e){
            return false;
        }
        return true;
    }

    protected File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        MainActivity activity = (MainActivity)getActivity();
        activity.setCurrentPhotoPath("file:" + image.getAbsolutePath());
        return image;
    }
    protected void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        MainActivity activity = (MainActivity)getActivity();
        File f = new File(activity.getCurrentPhotoPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.getActivity().sendBroadcast(mediaScanIntent);
    }
    private void setFullImageFromFilePath(String imagePath, ImageView imageView) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            addPhotoToGallery();
            MainActivity activity = (MainActivity)getActivity();
            setFullImageFromFilePath(activity.getCurrentPhotoPath(), previewImage);

        }else{
            //Image Capture Failed
            Toast.makeText(getActivity(),"Failed to initialize the camera",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_button: {
                if(isPhotoTaken()){
                    ((MainActivity)getActivity()).sendImageToServer();
                }
                break;
            }
            case R.id.cancel_button: {
                ((MainActivity)getActivity()).shiftFragment(new DefaultView());
                break;
            }
            case R.id.image_view:{
                takePhoto();
                break;
            }

        }
    }


}