package mak.aidev.imageuploader;


import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;

import mak.aidev.dataobjects.CostData;
import mak.aidev.dataobjects.Diagnosis;
import mak.aidev.dataobjects.ServerReply;
import mak.aidev.services.AiDevService;
import mak.aidev.services.AiDevServiceHandler;
import mak.aidev.services.CostPostListener;
import mak.aidev.services.DiagnosisListener;
import mak.aidev.views.DefaultView;
import retrofit.RestAdapter;

public class MainActivity extends AppCompatActivity implements CostPostListener, DiagnosisListener {
    private RestAdapter restAdapter;
    private AiDevService aiDevService;
    private AiDevServiceHandler aiDevServiceHandler;
    private ProgressDialog progressDialog;
    private CoordinatorLayout coordinatorLayout;
    private final static String CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath";
    private final static String CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI";
    private String mCurrentPhotoPath = null;
    private Uri mCapturedImageURI = null;
    private File imageFile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        restAdapter = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).
                setEndpoint(getString(R.string.server)).build();
        aiDevService = restAdapter.create(AiDevService.class);
        aiDevServiceHandler = new AiDevServiceHandler(aiDevService);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        shiftFragment(new DefaultView());
    }

    public void shiftFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void showMessage(String message){
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
    public void showError(String message){
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (mCurrentPhotoPath != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_PATH_KEY, mCurrentPhotoPath);
        }
        if (mCapturedImageURI != null) {
            savedInstanceState.putString(CAPTURED_PHOTO_URI_KEY, mCapturedImageURI.toString());
        }
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_PATH_KEY)) {
            mCurrentPhotoPath = savedInstanceState.getString(CAPTURED_PHOTO_PATH_KEY);
        }
        if (savedInstanceState.containsKey(CAPTURED_PHOTO_URI_KEY)) {
            mCapturedImageURI = Uri.parse(savedInstanceState.getString(CAPTURED_PHOTO_URI_KEY));
        }
        super.onRestoreInstanceState(savedInstanceState);
    }
    /**
     * Getters and setters.
     */
    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }
    public void setCurrentPhotoPath(String mCurrentPhotoPath) {
        this.mCurrentPhotoPath = mCurrentPhotoPath;
    }
    public Uri getCapturedImageURI() {
        return mCapturedImageURI;
    }
    public void setCapturedImageURI(Uri mCapturedImageURI) {
        this.mCapturedImageURI = mCapturedImageURI;
    }



    @Override
    public void onCostPost(ServerReply serverReply) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        String message = String.format("%s %s",serverReply.getStatus(), serverReply.getMessage());

            showMessage(message);

    }

    @Override
    public void onDiagnosisResult(Diagnosis diagnosis) {
        String messageData = String.format("HEALTHY:%s % CBB:%s %\n" +
                "CBSD:%s % CMD:%s %", diagnosis.getHealthy(), diagnosis.getCbb(),
                diagnosis.getCbsd(), diagnosis.getCmd());
        Toast.makeText(MainActivity.this, messageData, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNetworkFailure(String localizedMessage) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        showError(localizedMessage);
    }

    public void sendImageToServer(){
        imageFile = new File(getCurrentPhotoPath());
        aiDevServiceHandler.diagnoseImage(imageFile,this);

    }

    public void sendCostInformation(CostData costData) {
        aiDevServiceHandler.sendCostInformation(costData, this);
    }
}
