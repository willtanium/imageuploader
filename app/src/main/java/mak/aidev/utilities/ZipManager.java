package mak.aidev.utilities;

import android.net.Uri;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by william on 3/3/16.
 */
public class ZipManager {
    private static int BUFFER = 1024;
    public static void zip(List<Uri> _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (Uri filePath : _files) {

                FileInputStream fi = new FileInputStream(filePath.getPath());
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(filePath.getPath().substring(filePath.getPath()
                        .lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
